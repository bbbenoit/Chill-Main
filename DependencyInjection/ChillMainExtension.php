<?php

/*
 * Copyright (C) 2014-2016 Julien Fastré <julien.fastre@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\MainBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Chill\MainBundle\DependencyInjection\Widget\Factory\WidgetFactoryInterface;
use Chill\MainBundle\DependencyInjection\Configuration;

/**
 * This class load config for chillMainExtension.
 */
class ChillMainExtension extends Extension implements PrependExtensionInterface,
    Widget\HasWidgetFactoriesExtensionInterface
{
    /**
     * widget factory
     * 
     * @var WidgetFactoryInterface[]
     */
    protected $widgetFactories = array();
    
    public function addWidgetFactory(WidgetFactoryInterface $factory)
    {
        $this->widgetFactories[] = $factory;
    }
    
    /**
     * 
     * @return WidgetFactoryInterface[]
     */
    public function getWidgetFactories()
    {
        return $this->widgetFactories;
    }
    
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        // configuration for main bundle
        $configuration = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuration, $configs);
        
        $container->setParameter('chill_main.installation_name',
            $config['installation_name']);

        $container->setParameter('chill_main.available_languages',
            $config['available_languages']);
        
        $container->setParameter('chill_main.routing.resources', 
            $config['routing']['resources']); 
        
        $container->setParameter('chill_main.pagination.item_per_page',
            $config['pagination']['item_per_page']);
        
        // add the key 'widget' without the key 'enable'
        $container->setParameter('chill_main.widgets', 
            isset($config['widgets']['homepage']) ? 
                array('homepage' => $config['widgets']['homepage']):
                array()
                );

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $loader->load('services/logger.yml');
        $loader->load('services/repositories.yml');
        $loader->load('services/pagination.yml');
        
    }
    
    public function getConfiguration(array $config, ContainerBuilder $container)
    {
        return new Configuration($this->widgetFactories, $container);
    }
    
    
    public function prepend(ContainerBuilder $container) 
    {
        $bundles = $container->getParameter('kernel.bundles');
        //add ChillMain to assetic-enabled bundles
        if (!isset($bundles['AsseticBundle'])) {
            throw new MissingBundleException('AsseticBundle');
        }

        $asseticConfig = $container->getExtensionConfig('assetic');
        $asseticConfig['bundles'][] = 'ChillMainBundle';
        $container->prependExtensionConfig('assetic', 
                array('bundles' => array('ChillMainBundle')));
        
        //add installation_name and date_format to globals
        $chillMainConfig = $container->getExtensionConfig($this->getAlias());
        $config = $this->processConfiguration($this
                ->getConfiguration($chillMainConfig, $container), $chillMainConfig);
        $twigConfig = array(
            'globals' => array(
                'installation' => array(
                    'name' => $config['installation_name']),
                'available_languages' => $config['available_languages']
            ),
            'form_themes' => array('ChillMainBundle:Form:fields.html.twig')
        );
        $container->prependExtensionConfig('twig', $twigConfig);
        
        //add DQL function to ORM (default entity_manager)
        $container->prependExtensionConfig('doctrine', array(
           'orm' => array(
              'dql' => array(
                 'string_functions' => array(
                    'unaccent' => 'Chill\MainBundle\Doctrine\DQL\Unaccent'
                 )
              )
           )
        ));
        
        //add current route to chill main
        $container->prependExtensionConfig('chill_main', array(
           'routing' => array(
              'resources' => array(
                 '@ChillMainBundle/Resources/config/routing.yml'
              )
              
           )
        ));
        
        //add a channel to log app events
        $container->prependExtensionConfig('monolog', array(
            'channels' => array('chill')
        ));
    }
}
