<?php

namespace Chill\MainBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Chill\MainBundle\DependencyInjection\Widget\Factory\WidgetFactoryInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Chill\MainBundle\DependencyInjection\Widget\AddWidgetConfigurationTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;


/**
 * Configure the main bundle
 */
class Configuration implements ConfigurationInterface
{
    
    use AddWidgetConfigurationTrait;
    
    /**
     *
     * @var ContainerBuilder
     */
    private $containerBuilder;

    
    public function __construct(array $widgetFactories = array(), 
            ContainerBuilder $containerBuilder)
    {
        $this->setWidgetFactories($widgetFactories);
        $this->containerBuilder = $containerBuilder;
    }
    
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('chill_main');

        $rootNode
            ->children()
                ->scalarNode('installation_name')
                    ->cannotBeEmpty()
                    ->defaultValue('Chill')
                ->end() // end of scalar 'installation_name'
                ->arrayNode('available_languages')
                    ->defaultValue(array('fr'))
                    ->prototype('scalar')->end()
                ->end() // end of array 'available_languages'
                ->arrayNode('routing')
                    ->children()
                        ->arrayNode('resources')
                        ->prototype('scalar')->end()
                        ->end() // end of array 'resources'
                    ->end() // end of children
                ->end() // end of array node 'routing'
                ->arrayNode('pagination')
                    ->canBeDisabled()
                    ->children()
                        ->integerNode('item_per_page')
                        ->info('The number of item to show in the page result, by default')
                        ->min(1)
                        ->defaultValue(50)
                        ->end() // end of integer 'item_per_page'
                    ->end() // end of children
                ->end() // end of pagination
                ->arrayNode('widgets')
                    ->canBeEnabled()
                    ->canBeUnset()
                    ->children()
                         ->append($this->addWidgetsConfiguration('homepage', $this->containerBuilder))
                    ->end() // end of widgets/children
                ->end() // end of widgets
               ->end() // end of root/children
            ->end() // end of root
        ;

        
        return $treeBuilder;
    }
}
