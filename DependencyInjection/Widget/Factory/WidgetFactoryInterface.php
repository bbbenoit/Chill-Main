<?php

/*
 * Copyright (C) 2016 Julien Fastré <julien.fastre@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\MainBundle\DependencyInjection\Widget\Factory;


use Symfony\Component\Config\Definition\Builder\NodeBuilder;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Factory for creating configuration of widgets.
 * 
 * When you need a widget with some configuration, you should implements this
 * interface on a factory. The factory will add configuration to the bundle 
 * giving the places for your widget.
 * 
 * Using this interface, **you do not need** to define the service in your 
 * container configuration (`services.yml` files). 
 * 
 * Once the class is created, you should inject the factory inside the container
 * at compile time, in your `Bundle` class :
 * 
 * 
 * ```
 * namespace Chill\PersonBundle;
 *
 * use Symfony\Component\HttpKernel\Bundle\Bundle;
 * use Symfony\Component\DependencyInjection\ContainerBuilder;
 * use Chill\PersonBundle\Widget\PersonListWidgetFactory;
 * 
 * class ChillPersonBundle extends Bundle
 * {
 *      public function build(ContainerBuilder $container) 
 *   {
 *       parent::build($container);
 *       // register a widget factory into chill_main :
 *       $container->getExtension('chill_main')
 *           ->addWidgetFactory(new PersonListWidgetFactory());
 *   }
 * }
 * ```
 * 
 * 
 *
 */
interface WidgetFactoryInterface
{
    /**
     * configure options for the widget. Those options will be added in 
     * configuration in the bundle where the widget will be used.
     * 
     * @param type $place
     * @param NodeBuilder $node
     */
    public function configureOptions($place, NodeBuilder $node);
    
    /**
     * get the widget alias. This alias will be used in configuration (`config.yml`)
     */
    public function getWidgetAlias();
    
    /**
     * Create a definition for the service which will render the widget.
     * 
     * (Note: you can define the service by yourself, as other services, 
     * using the `AbstractWidgetFactory`)
     * 
     * @param ContainerBuilder $containerBuilder
     * @param type $place
     * @param type $order
     * @param array $config
     */
    public function createDefinition(ContainerBuilder $containerBuilder, $place, $order, array $config);
    
    /**
     * return the service id to build the widget.
     * 
     * @param ContainerBuilder $containerBuilder
     * @param string $place
     * @param float $order
     * @param array $config
     * 
     * @return string the service definition
     * 
     */
    public function getServiceId(ContainerBuilder $containerBuilder, $place, $order, array $config);
}
