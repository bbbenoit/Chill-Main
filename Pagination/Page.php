<?php

/*
 * Chill is a software for social workers
 * Copyright (C) 2016 Champs-Libres Coopérative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\MainBundle\Pagination;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * a page is an element of a pagination
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class Page implements PageInterface
{
    /**
     *
     * @var UrlGeneratorInterface
     */
    protected $urlGenerator;
    
    /**
     * the number of the current page
     *
     * @var int
     */
    protected $number;
    
    /**
     * The route for the current page
     *
     * @var string
     */
    protected $route;
    
    /**
     * Parameters for the route to the current page
     *
     * @var array
     */
    protected $routeParameters;
    
    /**
     * the number of item per page
     *
     * @var int
     */
    protected $itemPerPage;


    public function __construct(
          $number, 
          $itemPerPage,
          UrlGeneratorInterface $urlGenerator,
          $route,
          array $routeParameters
    ) {
        $this->urlGenerator = $urlGenerator;
        $this->number = $number;
        $this->itemPerPage = $itemPerPage;
        $this->route = $route;
        $this->routeParameters = $routeParameters;
    }
    
    public function generateUrl()
    {
        return $this->urlGenerator->generate($this->route, $this->routeParameters);
    }

    public function getFirstItemNumber()
    {
        return ($this->number - 1) * $this->itemPerPage;
    }

    public function getLastItemNumber()
    {
        return $this->number * $this->itemPerPage - 1;
    }

    public function getNumber()
    {
        return $this->number;
    }

}
