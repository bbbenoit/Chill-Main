<?php

/*
 * Chill is a software for social workers
 * Copyright (C) 2016 Champs-Libres Coopérative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\MainBundle\Pagination;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


/**
 * Standard paginator class.
 *
 * Represent a set of paginated pages;
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class Paginator implements PaginatorInterface
{
    /**
     * The number of total items
     *
     * @var int
     */
    protected $totalItems;
    
    /**
     * the number of items on a single page
     *
     * @var int
     */
    protected $itemPerPage;
    /**
     * The number of the current page
     *
     * @var int
     */
    protected $currentPageNumber;
    
    /**
     * the route of the pages
     *
     * @var string
     */
    protected $route;
    
    /**
     * the parameters of the route
     *
     * @var string[]
     */
    protected $routeParameters;
    
    /**
     * the generator for url
     *
     * @var UrlGeneratorInterface
     */
    protected $urlGenerator;
    
    /**
     * the key in the GET parameter to indicate the page number in
     * generated routes
     *
     * @var string
     */
    protected $pageKey;
    
    /**
     * the key in the GET parameter to indicate the number of item per page
     *
     * @var string
     */
    protected $itemPerPageKey;


    public function __construct(
          $totalItems,
          $itemPerPage,
          $currentPageNumber,
          $route,
          array $routeParameters,
          UrlGeneratorInterface $urlGenerator,
          $pageKey,
          $itemPerPageKey
    ) {
        $this->totalItems = $totalItems;
        $this->itemPerPage = $itemPerPage;
        $this->currentPageNumber = $currentPageNumber;
        $this->route = $route;
        $this->routeParameters = $routeParameters;
        $this->urlGenerator = $urlGenerator;
        $this->pageKey = $pageKey;
        $this->itemPerPageKey = $itemPerPageKey;
    }

    public function count()
    {
        return $this->countPages();
    }

    /**
     *
     * @return \Chill\MainBundle\Pagination\Page
     */
    public function getCurrentPage()
    {
        return $this->getPage($this->currentPageNumber);
    }

    public function getCurrentPageFirstItemNumber()
    {
        return $this->getCurrentPage()->getFirstItemNumber();
    }
    
    public function isCurrentPage(PageInterface $page)
    {
        return $page->getNumber() === $this->currentPageNumber;
    }

        public function getItemsPerPage()
    {
        return $this->itemPerPage;
    }
    
    public function setItemsPerPage($itemPerPage)
    {
        $this->itemPerPage = $itemPerPage;
    }

        public function getTotalItems()
    {
        return $this->totalItems;
    }

    public function countPages()
    {
        $nb = floor($this->totalItems / $this->itemPerPage);
        
        if ($this->totalItems % $this->itemPerPage > 0) {
            $nb++;
        }
        
        return $nb == 0 ? 1 : (int) $nb;
    }

    /**
     *
     * @return \Chill\MainBundle\Pagination\Page
     * @throws \RuntimeException if the next page does not exists
     */
    public function getNextPage()
    {
        if (!$this->hasNextPage()) {
            throw new \RuntimeException("this page has no next page");
        }
        
        return $this->getPage($this->currentPageNumber + 1);
    }

    /**
     *
     * @return \Chill\MainBundle\Pagination\Page
     * @throws \RuntimeException if the next page does not exists
     */
    public function getPreviousPage()
    {
        if (!$this->hasPreviousPage()) {
            throw new \RuntimeException("this page has no previous page");
        }
        
        return $this->getPage($this->currentPageNumber - 1);
    }

    /**
     *
     * @return bool
     */
    public function hasNextPage()
    {
        return $this->hasPage($this->currentPageNumber + 1);
    }

    /**
     *
     * @return bool
     */
    public function hasPreviousPage()
    {
        return $this->hasPage($this->currentPageNumber - 1);
    }
    
    public function hasPage($number)
    {
        return $number > 0 and
              $number <= $this->countPages();
    }

    
    /**
     *
     * @param type $number
     * @return \Chill\MainBundle\Pagination\Page
     */
    public function getPage($number) {
        if (!$this->hasPage($number)) {
            throw new \RuntimeException("The page with number $number does not "
                  . "exists");
        }
        
        return new Page(
              $number,
              $this->itemPerPage,
              $this->urlGenerator,
              $this->route,
              array_merge($this->routeParameters, array(
                 $this->pageKey => $number,
                 $this->itemPerPageKey => $this->itemPerPage
              ))
              );
    }

    public function getPagesGenerator()
    {
        for ($i = 1; $i <= $this->countPages(); $i++) {
            yield $this->getPage($i);
        }
    }

}
