<?php

namespace Chill\MainBundle\Templating\Widget;

use \Twig_Environment;

interface WidgetInterface
{
    public function render(Twig_Environment $env, $place, array $context, array $config);
}