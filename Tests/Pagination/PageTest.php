<?php

/*
 * Chill is a software for social workers
 * Copyright (C) 2016 Champs-Libres Coopérative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\MainBundle\Tests\Pagination;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Chill\MainBundle\Pagination\Page;

/**
 * Test the Page class
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class PageTest extends KernelTestCase
{
    protected $paginator;
    
    protected $prophet;
    
    public function setUp() {
        $this->prophet = new \Prophecy\Prophet;
        
        
    }
    
    /**
     * 
     * @param int $maxResult
     * @param int $itemPerPage
     * @param string $route
     * @param array $routeParameters
     * @return Page
     */
    protected function generatePage(
          $number = 1, 
          $itemPerPage = 10,
          $route = 'route',
          array $routeParameters = array()
    ) {
        $urlGenerator = $this->prophet->prophesize();
        $urlGenerator->willImplement(UrlGeneratorInterface::class);
        
        return new Page($number, $itemPerPage, $urlGenerator->reveal(), $route, 
              $routeParameters);
    }
    
    public function testPageNumber() {
        $page = $this->generatePage(1);
        
        $this->assertEquals(1, $page->getNumber());
    }
    
    /**
     * return a set of element to testGetFirstItemNumber
     * 
     * the set contains : 
     * - the page number ;
     * - the number of item per page ;
     * - the expected first item number
     * 
     * @return array
     */
    public function generateGetFirstItemNumber() {
        return array(
           [1, 10, 0],
           [2, 10, 10]
        );
    }
    
    /**
     * 
     * @param int $number
     * @param int $itemPerPage
     * @param int $expectedItemPerPage
     * @dataProvider generateGetFirstItemNumber
     */
    public function testGetFirstItemNumber(
          $number, 
          $itemPerPage, 
          $expectedItemPerPage
    ) {
        $page = $this->generatePage($number, $itemPerPage);
        
        $this->assertEquals($expectedItemPerPage, $page->getFirstItemNumber());
    }
    
    /**
     * return a set of element to testGetLastItemNumber
     * 
     * the set contains : 
     * - the page number ;
     * - the number of item per page ;
     * - the expected last item number
     * 
     * @return array
     */
    public function generateGetLastItemNumber() {
        return array(
           [1, 10, 9],
           [2, 10, 19]
        );
    }
    
    /**
     * 
     * @param int $number
     * @param int $itemPerPage
     * @param int $expectedItemPerPage
     * @dataProvider generateGetLastItemNumber
     */
    public function testGetLastItemNumber(
          $number, 
          $itemPerPage, 
          $expectedItemPerPage
    ) {
        $page = $this->generatePage($number, $itemPerPage);
        
        $this->assertEquals($expectedItemPerPage, $page->getLastItemNumber());
    }
    
    
}
