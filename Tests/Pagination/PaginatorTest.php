<?php

/*
 * Chill is a software for social workers
 * Copyright (C) 2016 Champs-Libres Coopérative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\MainBundle\Tests\Pagination;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Chill\MainBundle\Pagination\Paginator;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Test the paginator class
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class PaginatorTest extends KernelTestCase
{
    protected $paginator;
    
    protected $prophet;
    
    public function setUp() 
    {
        $this->prophet = new \Prophecy\Prophet;
    }
    
    /**
     * 
     * @param int $maxResult
     * @param int $itemPerPage
     * @param string $route
     * @param array $routeParameters
     * @return Paginator
     */
    protected function generatePaginator(
          $totalItems,
          $itemPerPage,
          $currentPageNumber = 1,
          $route = '',
          array $routeParameters = array()
    ) {
        $urlGenerator = $this->prophet->prophesize();
        $urlGenerator->willImplement(UrlGeneratorInterface::class);
        
        return new Paginator(
              $totalItems, 
              $itemPerPage,
              $currentPageNumber,
              $route,
              $routeParameters,
              $urlGenerator->reveal(),
              'page',
              'item_per_page'
            );
    }
    
    /**
     * generate a set of pages with different maxItem, itemPerPage, and
     * expected page number
     * 
     * @return array
     */
    public function generatePageNumber() 
    {
        return array(
           [12, 10, 2],
           [20, 10, 2],
           [21, 10, 3],
           [19, 10, 2],
           [1, 10, 1],
           [0, 10, 1],
           [10, 10, 1]
        );
    }
    
    /**
     * Test that the countPages method (and his alias `count`) return
     * valid results.
     * 
     * @param int $totalItems
     * @param int $itemPerPage
     * @param int $expectedPageNumber
     * @dataProvider generatePageNumber
     */
    public function testPageNumber($totalItems, $itemPerPage, $expectedPageNumber) 
    {
        $paginator = $this->generatePaginator($totalItems, $itemPerPage);
        
        $this->assertEquals($expectedPageNumber, $paginator->countPages());
        $this->assertEquals($expectedPageNumber, count($paginator));
    }
    
    /**
     * generate an array with a set of page with :
     * - total items ;
     * - item per page ;
     * - current page number ;
     * - expected hasNextPage result
     * 
     * @return array
     */
    public function generateHasNextPage() 
    {
        return array(
          [10, 10, 1, false],
          [20, 10, 1, true],
          [12, 10, 1, true],
          [12, 10, 2, false]
        );
    }
    
    /**
     * 
     * @param int $totalItems
     * @param int $itemPerPage
     * @param int $currentPage
     * @param bool $expectedHasNextPage
     * @dataProvider generateHasNextPage
     */
    public function testHasNextPage(
          $totalItems, 
          $itemPerPage, 
          $currentPage, 
          $expectedHasNextPage
    ) {
        $paginator = $this->generatePaginator($totalItems, $itemPerPage, $currentPage);
        
        $this->assertEquals($expectedHasNextPage, $paginator->hasNextPage());
    }
    
    /**
     * generate an array with a set of page with :
     * - total items ;
     * - item per page ;
     * - current page number ;
     * - expected hasPreviousPage result
     * 
     * @return array
     */
    public function generateHasPreviousPage() 
    {
        return array(
          [10, 10, 1, false],
          [20, 10, 1, false],
          [12, 10, 1, false],
          [12, 10, 2, true],
        );
    }
    
    /**
     * 
     * @param int $totalItems
     * @param int $itemPerPage
     * @param int $currentPage
     * @param bool $expectedHasPreviousPage
     * @dataProvider generateHasPreviousPage
     */
    public function testHasPreviousPage(
          $totalItems, 
          $itemPerPage, 
          $currentPage, 
          $expectedHasNextPage
    ) {
        $paginator = $this->generatePaginator($totalItems, $itemPerPage, $currentPage);
        
        $this->assertEquals($expectedHasNextPage, $paginator->hasPreviousPage());
    }
    
    public function generateHasPage()
    {
        return array(
           [10, 10, -1, false],
           [12, 10, 1,  true],
           [12, 10, 2,  true],
           [12, 10, 3,  false]
        );
    }
    
    /**
     * test the HasPage function
     * 
     * 
     * @param int $totalItems
     * @param int $itemPerpage
     * @param int $pageNumber
     * @param bool $expectedHasPage
     * @dataProvider generateHasPage
     */
    public function testHasPage($totalItems, $itemPerpage, $pageNumber, 
          $expectedHasPage)
    {
        $paginator = $this->generatePaginator($totalItems, $itemPerpage);
        
        $this->assertEquals($expectedHasPage, $paginator->hasPage($pageNumber));
    }
    
    public function testGetPage()
    {
        $paginator = $this->generatePaginator(105, 10);
        
        $this->assertEquals(5, $paginator->getPage(5)->getNumber());
    }
    
    public function testPagesGenerator()
    {
        $paginator = $this->generatePaginator(105, 10);
        
        $generator = $paginator->getPagesGenerator();
        
        $i = 1;
        foreach($generator as $page) {
            $this->assertEquals($i, $page->getNumber(),
                  "assert that the current page number is $i");
            $i++;
        }
        
        $this->assertEquals(11, $page->getNumber(), 
              "assert that the last page number is 11");
    }
}
